<?php

/**
 * @file
 * Provides example functionality.
 */

/**
 * Available variables.
 *
 * $title string -
 * $created string -
 */
?>
<h2><?php print $title; ?></h2>
<p>Created: <?php print $created; ?></p>
